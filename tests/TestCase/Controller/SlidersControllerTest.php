<?php
namespace Slider\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Slider\Controller\SlidersController;

/**
 * Slider\Controller\SlidersController Test Case
 */
class SlidersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.sliders'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
