<?php
namespace Slider\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Slider\Controller\Admin\LayersController;

/**
 * Slider\Controller\Admin\LayersController Test Case
 */
class LayersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.layers'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
