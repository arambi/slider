<?php
namespace Slider\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Slider\Controller\Admin\SlidersController;

/**
 * Slider\Controller\Admin\SlidersController Test Case
 */
class SlidersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.sliders'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
