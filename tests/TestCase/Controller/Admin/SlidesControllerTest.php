<?php
namespace Slider\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Slider\Controller\Admin\SlidesController;

/**
 * Slider\Controller\Admin\SlidesController Test Case
 */
class SlidesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.slides'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
