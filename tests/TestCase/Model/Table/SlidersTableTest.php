<?php
namespace Slider\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Slider\Model\Table\SlidersTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Slider\Model\Table\SlidersTable Test Case
 */
class SlidersTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.sliders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sliders') ? [] : ['className' => 'Slider\Model\Table\SlidersTable'];
        $this->Sliders = TableRegistry::get('Sliders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sliders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Sliders);
        $this->assertCrudDataIndex( 'index', $this->Sliders);
      }
}
