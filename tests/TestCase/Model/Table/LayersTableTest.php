<?php
namespace Slider\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Slider\Model\Table\LayersTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Slider\Model\Table\LayersTable Test Case
 */
class LayersTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.slider.layers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Layers') ? [] : ['className' => 'Slider\Model\Table\LayersTable'];
        $this->Layers = TableRegistry::get('Layers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Layers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Layers);
        $this->assertCrudDataIndex( 'index', $this->Layers);
      }
}
