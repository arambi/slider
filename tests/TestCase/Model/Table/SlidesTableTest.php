<?php
namespace Slider\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Slider\Model\Table\SlidesTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Slider\Model\Table\SlidesTable Test Case
 */
class SlidesTableTest extends CrudTestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
      'plugin.slider.slides',
      'plugin.slider.layers'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $config = TableRegistry::exists('Slides') ? [] : ['className' => 'Slider\Model\Table\SlidesTable'];
    $this->Slides = TableRegistry::get('Slides', $config);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->Slides);

    parent::tearDown();
  }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Slides);
        $this->assertCrudDataIndex( 'index', $this->Slides);
      }
}
