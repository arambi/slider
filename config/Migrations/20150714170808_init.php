<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
  /**
   * Change Method.
   *
   * Write your reversible migrations using this method.
   *
   * More information on writing migrations is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
    
  public function change()
  {
   $this->table( 'slider_slides')->addColumn( 'has_url', 'boolean', array( 'null' => true, 'default' => null))->update();
  }
  */
 
  public function up()
    {
      $sliders = $this->table( 'slider_sliders');
      $sliders
        ->addColumn( 'title', 'string', array( 'limit' => 64, 'default' => null))
        ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
        ->addColumn( 'height', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'width', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'auto_height', 'boolean', ['null' => true, 'default' => 0])
        ->addColumn( 'layout', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'view', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))

        ->addColumn( 'parallax_mode', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'speed', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'dir', 'string', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'autoplay', 'boolean', array( 'null' => true, 'default' => null))
        ->addColumn( 'slider_loop', 'boolean', array( 'null' => true, 'default' => null))
        ->addColumn( 'shuffle', 'boolean', array( 'null' => true, 'default' => null))

        ->addColumn( 'created', 'datetime', array('default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['salt'])
        ->save();

      $slides = $this->table( 'slider_slides');
      $slides
        ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
        ->addColumn( 'slider_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'has_url', 'boolean', array( 'null' => true, 'default' => null))
        ->addColumn( 'url', 'string', array( 'limit' => 128, 'null' => true, 'default' => null))
        ->addColumn( 'url_target', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'fill_mode', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'photo', 'text', array( 'null' => true, 'default' => null))
        ->addColumn( 'delay', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'created', 'datetime', array( 'default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['slider_id'])
        ->addIndex( ['salt'])
        ->save();

      $layers = $this->table( 'slider_layers');
      $layers
        ->addColumn( 'title', 'string', array( 'limit' => 64, 'default' => null))
        ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
        ->addColumn( 'slide_id', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'layer_type', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'pos_top', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'pos_left', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'offset_x', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'offset_y', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'origin', 'string', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'effect', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'duration', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'position', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'delay', 'integer', array( 'null' => true, 'default' => null))
        ->addColumn( 'ease', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'parallax_effect', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_name', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'transition_ease', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'transition_offset_x', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_offset_y', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_offset_z', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_scale_x', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_scale_y', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'transition_fade', 'boolean', array( 'null' => true, 'default' => null))

        ->addColumn( 'padding_left', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'padding_right', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'padding_top', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'padding_bottom', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'foreground_color', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'background_color', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'border_color', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'border_top_width', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'border_bottom_width', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'border_left_width', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'border_right_width', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'border_style', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
        ->addColumn( 'border_round', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))

        ->addColumn( 'width', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'text_shadow_angle', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'text_shadow_distance', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'text_shadow_blur', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'text_shadow_opacity', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
        ->addColumn( 'line_height', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))

        ->addColumn( 'body', 'text', array( 'null' => true, 'default' => null))
        ->addColumn( 'photo', 'text', array( 'null' => true, 'default' => null))
        ->addColumn( 'created', 'datetime', array( 'default' => null))
        ->addColumn( 'modified', 'datetime', array( 'default' => null))
        ->addIndex( ['slide_id'])
        ->addIndex( ['salt'])
        ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      /*
      $this->dropTable( 'slider_sliders');
      $this->dropTable( 'slider_slides');
      $this->dropTable( 'slider_layers');
      */
    }
}
