<?php 
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Block\Lib\BlocksRegistry;

Access::add( 'sliders', [
  'name' => 'Sliders',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Sliders',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Sliders',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Sliders',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Sliders',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Sliders',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Sliders',
  'parentName' => 'Sliders',
  'plugin' => 'Slider',
  'controller' => 'Sliders',
  'action' => 'index',
  'icon' => 'fa  fa-caret-square-o-right',
]);



ActionCollection::set( 'sliders', [
  'label' => 'Sliders',
  'plugin' => 'Slider',
  'controller' => 'Sliders',
  'action' => 'index',
  'icon' => 'fa fa-square',
  'actions' => [
    'view' => '/:slug'
  ]
]);



Access::add( 'slides', [
  'name' => 'Slides',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Slides',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Slides',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Slides',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Slides',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Slides',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);



Access::add( 'layers', [
  'name' => 'Layers',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Layers',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Layers',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Layers',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Layers',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slider',
          'controller' => 'Layers',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);







// Bloque Slider
BlocksRegistry::add( 'slider', [
    'key' => 'slider',
    'title' => __d( 'admin', 'Slider'),
    'icon' => 'fa fa-caret-square-o-right ',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Slider\\Model\\Block\\SliderBlock',
    'cell' => 'Slider.Slider::display',
    'blockView' => 'Slider/blocks/slider'
]);
