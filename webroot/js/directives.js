;(function(){
  function sliderAddSlide( $timeout, $http) {
    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        element.click(function(){
          $http.get( '/admin/slider/sliders/create.json').success( function( data){
            scope.content[scope.field.key].push( data.content);
            scope.currentSlide = scope.content[scope.field.key].length - 1;
          })
          
          $timeout(function(){
            scope.$apply();
          })
          return false;
        })
      }
    }
  }

  function sliderAddLayer( $timeout, $http) {
    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        element.click(function(){
          if( !scope.add_action){
            return;
          }
          $http.get( '/admin/slider/layers/create/' + scope.add_action + '.json').success( function( data){
            if( !scope.content[scope.field.key]){
              scope.content[scope.field.key] = [];
            }
            
            scope.content[scope.field.key].push( data.content);
            scope.currentLayer = scope.content[scope.field.key].length - 1;
            $timeout(function(){
              scope.$apply();
            })
          })
               
          return false;
        })
      }
    }
  }

  function sliderSelectLayerType( $timeout, $http) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          scope.$parent.add_action = scope.option;
          scope.$parent.add_label = scope.name;
          $timeout(function(){
            scope.$apply();
          })      
        })
      }
    }
  }

  function sliderChange( $timeout) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          scope.$parent.currentSlide = scope.$index;
          $('li', $(element).parent().parent()).removeClass( 'active');
          $(element).parent().addClass( 'active');
          $timeout(function(){
            scope.$apply();
          })
          return false;
        })
      }
    }
  }

  function sliderLayer( $timeout) {
    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        
        if( !scope.sortableLayer){
          scope.sortableLayer = {
            items: '> li',
            distance: 50,
            stop: function(e, ui) {
              var layers = ngModel.$viewValue;
              var position = 99;
              for( var i = 0; i < layers.length; i++){
                layers [i].position = position;
                position--;
              }
              ngModel.$setViewValue( layers);
            }
          }
        }
      }
    }
  }


/**
 * lyrRangeSlider - Directive for Layers
 */
function lyrRangeSlider( $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: false,
        link: function (scope, elem, attrs, ngModel) {
          $timeout( function(){
            var model = ngModel.$viewValue;
            var from = model.delay; 
            var to = model.duration || 3000; 
            var options = {
              min: 0,
              max: 5000,
              from: from,
              to: to,
              type: 'double',
              prefix: '',
              maxPostfix: '',
              prettify: false,
              hasGrid: true,
              hide_min_max: true,
              hide_from_to: true
            };

            options.onChange = function( data){
              var model = ngModel.$viewValue;
              model.delay = data.fromNumber;
              model.duration = data.toNumber;
              ngModel.$setViewValue( model);
            }
            
            elem.ionRangeSlider( options);
          })
        }
    }
}


  function layerChange( $timeout) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          scope.$parent.currentLayer = scope.$index;
          $timeout(function(){
            scope.$apply();
          })
          $('li', $(element).parent().parent()).removeClass( 'active');
          $(element).parent().addClass( 'active');
          return false;
        })
      }
    }
  }

  function layerDraggable() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          element.draggable({
            cursor: "move",
            stop: function (event, ui) {
              var container = element.parent( '.slide-preview-cont');
              var model = ngModel.$viewValue;
              // model.pos_top = (ui.position.top * 100) / container.height();
              // model.pos_left = (ui.position.left * 100) / container.width();
              model.pos_top = Math.round(ui.position.top);
              model.pos_left = Math.round(ui.position.left);
              ngModel.$setViewValue( model);
            }
          });
        }
    };
  };

  function lyrBoxStyle() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          scope.$watchCollection( attrs.ngModel, function(){
            var model = ngModel.$viewValue;
            element.css( 'background-color', model.background_color);
            element.css( 'padding', model.padding_top + 'px ' 
                                    + model.padding_right + 'px ' 
                                    + model.padding_bottom + 'px '
                                    + model.padding_left + 'px ');
          })
        }
    };
  }

  /**
   *
   * Pass all functions into module
   */
  angular
      .module('admin')
      .directive( 'sliderAddSlide', sliderAddSlide)
      .directive( 'sliderAddLayer', sliderAddLayer)
      .directive( 'sliderChange', sliderChange)
      .directive( 'layerChange', layerChange)
      .directive( 'sliderSelectLayerType', sliderSelectLayerType)
      .directive( 'sliderLayer', sliderLayer)
      .directive( 'layerDraggable', layerDraggable)
      .directive( 'lyrRangeSlider', lyrRangeSlider)
      .directive( 'lyrBoxStyle', lyrBoxStyle)
  ;
})()

