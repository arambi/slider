<?php
namespace Slider\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;


/**
 * Slider cell
 */
class SliderCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $query = TableRegistry::get( 'Slider.Sliders')->find();
    
    $query->contain([
      'Slides' => [
        'Layers'
      ]
    ]);

    $query->where( ['Sliders.id' => $block->parent_id]);
    $slider = $query->first();
    $this->set( compact( 'slider'));
  }
}
