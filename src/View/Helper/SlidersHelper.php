<?php
namespace Slider\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cofree\View\Helper\StylesHelperTrait;
use \ArrayObject;

/**
 * Sliders helper
 */
class SlidersHelper extends Helper
{
  use StylesHelperTrait;

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public $helpers = ['Html', 'Upload.Uploads'];

  /**
   * Template de estilos 
   * El key es el nombre en la base de datos y el value es el template de cómo se escribirá
   * Si el template es tipo función, en lugar de hacer un str_replace, se llamará a esa función
   * @var [type]
   */
  public $styles = [
    'pos_left' => 'left: %spx',
    'pos_top' => 'top: %spx',
    'padding_left' => 'padding-left: %spx',
    'padding_right' => 'padding-right: %spx',
    'padding_top' => 'padding-top: %spx',
    'padding_bottom' => 'padding-bottom: %spx',
    'background_color' => 'background-color: %s',

    'border_color' => 'border-color: %spx',
    'border_top_width' => 'border-top-width: %spx',
    'border_bottom_width' => 'border-bottom-width: %spx',
    'border_left_width' => 'border-left-width: %spx',
    'border_right_width' => 'border-right-width: %spx',
    'border_style' => 'border-style: %s',
    'border_round' => 'borderRadius()',

    'foreground_color' => 'color: %s',
    'width' => 'width: %spx',

    'text_shadow_angle' => 'textShadow()',
    'line_height' => 'lineHeight()',
  ];

  public function layer( $layer)
  {
    $attrs = ['class' => 'ms-layer'];
    $attrs ['data-delay'] = $layer->delay;
    $attrs ['data-duration'] = $layer->duration - $layer->delay;
    $attrs ['data-ease'] = $layer->transition_ease;
    $attrs ['data-effect'] = !empty( $layer->transition_name) ? $layer->transition_name : 'fade';
    $attrs ['data-parallax'] = $layer->parallax_effect;

    $attrs ['style'] = $this->styles( $layer);

    if( $layer->layer_type == 'text')
    {
      $html = $layer->body;
      $pattern = '/font-size:(.*?)px/i';
      $callback = create_function( '$m', '$i = (($m[1] * 100) / 10) / 100; return "font-size:". $i. "rem";');
      $html = preg_replace_callback( $pattern, $callback, $html);
    }

    if( $layer->layer_type == 'image')
    {
      $html = $this->Uploads->image( $layer->photo, 'org');
    }

    return $this->Html->tag( 'div', $html, $attrs);
  }


  /**
 * Devuelve los atributos de un block, es decir, el class y el style
 * 
 * @param  Block\Model\Entity\Block $block
 * @return string HTML        
 */
  public function styles( $layer)
  {    
    $styles = new ArrayObject([]);

    foreach( $this->styles as $field => $prop)
    {
      $this->getPropertyValue( $layer, $field, $prop, $styles);
    }

    return implode( ';', (array)$styles);
  }

  public function lineHeight( $block, $value, $styles)
  {
    $styles [] = 'line-height: '. ($value / 100) . 'em';
  }
}
