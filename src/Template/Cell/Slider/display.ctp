<? $this->loadHelper( 'Slider.Sliders') ?>
<? $id = rand(0,999999) ?>
<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider_<?= $id ?>">
    
    <? foreach( $slider->slides as $slide): ?>
      <div class="ms-slide" data-fill-mode="<?= $slide->fill_mode ?>">     
          <!-- slide background -->
          <? if( !empty( $slide->photo)): ?>
              <?= $this->Uploads->image( $slide->photo) ?>
          <? endif ?>                    

          <? foreach( $slide->layers as $layer): ?>
              <?= $this->Sliders->layer( $layer) ?>
          <? endforeach ?>
      </div> 
    <? endforeach ?>
      
    <!-- end of slide -->
</div>

<? $this->Buffer->scriptStart() ?>

<script type="text/javascript">
  $('#masterslider_<?= $id ?>').masterslider({
    width: <?= $slider->width ?>,
    height: <?= $slider->height ?>,
    // more options...
    autoHeight: <?= $slider->auto_height ? 'true' : 'false' ?>,
    autoplay: <?= $slider->autoplay ? 'true' : 'false' ?>,
    loop: <?= $slider->loop ? 'true' : 'false' ?>,
    shuffle: <?= $slider->shuffle_order ? 'true' : 'false' ?>,
    view: '<?= $slider->view ?>',
    layout: '<?= $slider->layout ?>',
    parallaxMode: '<?= $slider->parallax_mode ?>'
    <? if( count( $slider->slides) > 1): ?>
        , controls : {
          arrows : {autohide:false},
          bullets : {
            autohide: false
          }
        }
    <? else: ?>
      , swipe: false
    <? endif ?>
    
    
  });
</script>

<? $this->Buffer->scriptEnd() ?>