<div slider-layer ng-model="content[field.key]" class="ember-view msp-timeline-cont" ng-init="currentLayer = 0">
  <h2><?= __d( 'admin', 'Capas') ?></h2>
  <div class="ember-view msp-addlayer">
  <button ng-init="add_action = false" slider-add-layer ng-model="option" cf-target="content[field.key]" data-ember-action="36" class="btn"><i class="fa fa-plus"></i></button>
    <div class="msp-addlayer-dd btn-group">
      <button data-toggle="dropdown" class="btn btn-w-m btn-white dropdown-toggle">{{ add_label }} <span class="caret"></span></button>
      <ul class="dropdown-menu">
        <li ng-repeat="(option, name) in crudConfig.view.fields.layer_type.options">
          <a slider-select-layer-type class="dd-option">
            <input class="dd-option-value" value="text" type="hidden"><i class="fa fa-text"></i>
            <label style="line-height: 25px;" class="dd-option-text"><span class="msp-layer-icon msp-layer-icon-{{ option }}"></span> {{ name }}</label>
          </a>
        </li>
      </ul>
    </div>
  </div>

  <div class="clearfix" class="ember-view msp-timeline-cont">
    <div class="msp-tl-headbar">
      <div class="msp-tl-controls">

      </div>
      <div class="msp-tl-timeruler-cont">

      </div>
    </div>

    <!-- listado de layers -->
    <div class="lyr-cont clearfix">
      <ul ui-sortable="sortableLayer" class="ui-sortable" ng-model="content[field.key]">
        <li class="ui-sortable-handle" ng-class="{active: $index == 0}" ng-repeat="_content in content[field.key]">
          <div class="lyr-label lyr-icon" layer-change>
            <div class="lyr-label-title"><span class="lyr-icon lyr-icon-{{ _content.layer_type }}"></span> {{ _content.title }}</div>
            <div class="lyr-label-buttons">
              <a tooltip="<?= __d( 'admin', 'Borrar capa') ?>" confirm-delete 
                  cf-destroy="scope.content[scope.field.key].splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></a>
            </div>
          </div>
          <div class="lyr-duration">
            <div lyr-range-slider ng-model="_content"></div>
          </div>
        </li>
      </ul>
    </div>
    <!-- listado de layers -->
  </div>

  <div ng-init="crudConfig = field.crudConfigAdd">
    <div ng-repeat="_content in content[field.key]">
      <div class="tabs-container" ui-tabs ng-show="currentLayer == $index" ng-init="content = _content">
        <tabset>
          <!-- Contenido -->
          <tab heading="<?= __d( 'admin', 'Contenido') ?>">
            <div class="panel-body">
              <input ng-model="content.id" type="hidden" class="form-control">
              <crud-field field="title"></crud-field>
              <crud-field ng-show="content.layer_type == 'image'" field="photo"></crud-field>
              <crud-field ng-show="content.layer_type == 'text'" field="body"></crud-field>
            </div>
          </tab>

          <!-- Transición -->
          <tab heading="<?= __d( 'admin', 'Transición') ?>">
            <div class="clearfix">
              <crud-field field="transition_name"></crud-field>
              <crud-field field="transition_ease"></crud-field>
              <crud-field field="parallax_effect"></crud-field>
              <h3><?= __d( 'admin', 'Desplazamiento') ?></h3>
              <div class="row">
                <div class="pull-left">
                  <crud-field field="transition_offset_x"></crud-field>
                </div>
                <div class="pull-left">
                  <crud-field field="transition_offset_y"></crud-field>
                </div>
                <div class="pull-left">
                  <crud-field field="transition_offset_z"></crud-field>
                </div>
              </div>
              
            </div>
          </tab>

          <!-- Estilo -->
          <tab heading="<?= __d( 'admin', 'Estilo') ?>">
            <div class="clearfix">
              <div class="pull-left"><crud-field field="line_height"></crud-field></div>
              <div class="pull-left"><crud-field field="width"></crud-field></div>
              <div class="pull-left"><crud-field field="background_color"></crud-field></div>
              <div class="pull-left"><crud-field field="foreground_color"></crud-field></div>
            </div>
            <div class="clearfix">
              <h3><?= __d( 'admin', 'Espacios') ?></h3>
              <div class="pull-left">
                <crud-field field="padding_top"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="padding_bottom"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="padding_left"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="padding_right"></crud-field>
              </div>
            </div>

            <div class="clearfix">
              <h3><?= __d( 'admin', 'Sombra de texto') ?></h3>
              <div class="pull-left">
                <crud-field field="text_shadow_angle"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="text_shadow_distance"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="text_shadow_blur"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="text_shadow_opacity"></crud-field>
              </div>
            </div>

            <div class="clearfix">
              <h3><?= __d( 'admin', 'Borde') ?></h3>
              <div class="pull-left">
                <crud-field field="border_color"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_top_width"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_bottom_width"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_left_width"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_right_width"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_style"></crud-field>
              </div>
              <div class="pull-left">
                <crud-field field="border_round"></crud-field>
              </div>
            </div>
          </tab>
        </tabset>
      </div>
    </div>
  </div>
</div>
