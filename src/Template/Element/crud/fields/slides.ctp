<div ng-init="currentSlide = 0">
  <h2><?= __d( 'admin', 'Slides') ?></h2>
  
  <div class="clearfix">
    <ul ui-sortable="sortableLayer" class="ui-sortable sld-thumbails" ng-model="content[field.key]">
      <li ng-repeat="_content in content[field.key]" ng-class="{active: $index == 0}">
        <img ng-src="{{ _content.photo.paths.square }}" slider-change />
        <div class="sld-thumbails-icons">
          <a tooltip="<?= __d( 'admin', 'Borrar slide') ?>" confirm-delete 
            cf-destroy="scope.content[scope.field.key].splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></a>
        </div>
      </li>
      <li slider-add-slide ng-model="option" cf-target="content[field.key]" class="sld-add"><i class="fa fa-plus"></i></li>
    </ul>
  </div>
  <div ng-repeat="_content in content[field.key]">
    <div ng-show="currentSlide == $index" ng-init="content = _content">
      <input ng-model="content.id" type="hidden" class="form-control">

      <!-- SLIDE PREVIEW -->
      <div class="slide-preview-cont clearfix" style="width:{{ $parent.content.width }}px; height:{{ $parent.content.height }}px">
        <img ng-src="{{ _content.photo.paths.big }}"  />
        <div ng-init="content = layer" data-drag="true" layer-draggable ng-model="layer" class="slide-layer" ng-repeat="layer in content.layers" style="
          z-index: {{ layer.position }};
          top: {{ layer.pos_top }}px; 
          left: {{ layer.pos_left }}px; 
          width: {{ layer.width }}px; 
          color: {{ layer.foreground_color }}; 
          background-color: {{ layer.background_color }};
          padding-left: {{ layer.padding_left }}px;
          padding-right: {{ layer.padding_right }}px;
          padding-top: {{ layer.padding_top }}px;
          padding-bottom: {{ layer.padding_bottom }}px;
          border-color: {{ layer.border_color }};
          border-top-width: {{ layer.border_top_width }}px;
          border-bottom_width: {{ layer.border_bottom_width }}px;
          border-left-width: {{ layer.border_left_width }}px;
          border-right-width: {{ layer.border_right_width }}px;
          border-style: {{ layer.border_style }};
          line-height: {{ layer.line_height/100 }}em;
          ">
          <img ng-if="layer.layer_type == 'image'" ng-src="{{ layer.photo.paths.org }}" />
          <div ng-model="layer" ng-if="layer.layer_type == 'text'" ng-bind-html="trustAsHtml( layer.body)"></div>
        </div>
      </div>
      <!-- /SLIDE PREVIEW -->

      <cr-tabs columns="field.crudConfigAdd.view.columns"></cr-tabs>
      <div ng-show="currentColumn == $index" ng-repeat="column in field.crudConfigAdd.view.columns">
        <div class="ibox float-e-margins" ng-repeat="box in column.box">
          <div ng-repeat="_field in box.elements">
            <ng-include ng-init="field = _field; content = _content; crudConfig = field.crudConfigAdd" ng-show="{{ _field.show }}" src="_field.template + '.html'"></ng-include>
          </div>
        </div>        
      </div>
    </div>
  </div>
</div>
