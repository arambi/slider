<?php
namespace Slider\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Slider\Model\Entity\Slider;

/**
 * Sliders Model
 */
class SlidersTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'slider_sliders');
    $this->displayField( 'title');
    $this->primaryKey( 'id');
    $this->addBehavior( 'Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // Asociations
    $this->hasMany( 'Slides', [
      'className' => 'Slider.Slides',
      'foreignKey' => 'slider_id'
    ]);

    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        // 'photo'
      ]
    ]);

    // CRUD Config
    $this->crud->associations( ['Slides', 'Layers']);

    $this->crud->addJsFiles([
      '/slider/js/directives.js',
    ]);

    $this->crud->addJsFiles([
      '/slider/css/slides.css',
    ]);

    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'height' => [
          'label' => __d( 'admin', 'Altura'),
          'type' => 'numeric',
          'range' => [50, 800, 1],
          'spin' => true,
        ],
        'width' => [
          'label' => __d( 'admin', 'Anchura'),
          'type' => 'numeric',
          'range' => [50, 1300, 1],
          'spin' => true,
        ],
        
        'auto_height' => __d( 'admin', 'Altura automatica'),
        'parallax_mode' => [
          'label' => __d( 'admin', 'Modo Parallax'),
          'type' => 'select',
          'options' => [
            'mouse' => 'Al mover el ratón',
            'swipe' => 'Al mover el dedo (tablets)',
          ],
          'help' => __d( 'admin', 'Indica el modo en que actua el parallax (si se especifica en las capas')
        ],
        'speed' => [
          'label' => __d( 'admin', 'Velocidad de cambio'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
          'help' => __d( 'admin', 'La velocidad de cambio de las diapositivas')
        ],
        'dir' => [
          'label' => __d( 'admin', 'Dirección de cambio'),
          'type' => 'select',
          'options' => [
            'h' => 'Horizontal',
            'v' => 'Vertical'
          ],
          'help' => __d( 'admin', 'Dirección de cambio de las diapositivas')
        ],
        'autoplay' => [
          'label' => __d( 'admin', 'Inicio automático'),
          'help' => __d( 'admin', 'Si se indica esta opción, el pase de diapositivas se iniciará automáticamente')
        ],
        'slider_loop' => [
          'label' => __d( 'admin', 'Loop'),
          'help' => __d( 'admin', 'Si se indica esta opción, el pase de diapositivas comenzará de nuevo una vez terminado'),
        ],
        'shuffle' => [
          'label' => __d( 'admin', 'Orden aleatorio'),
          'help' => __d( 'admin', 'Si se indica esta opción, el orden pase de diapositivas será aleatorio')
        ],
        'layout' => [
          'label' => __d( 'admin', 'Tipo'),
          'type' => 'select',
          'options' => [
            'boxed' => __d( 'admin', 'En caja'),
            'fullwidth' => __d( 'admin', 'Ancho completo'),
            'fullscreen' => __d( 'admin', 'Ventana completo'),
            'fillwidth' => __d( 'admin', 'Adaptable'),
            'autofill' => __d( 'admin', 'Auto'),
            'partialview' => __d( 'admin', 'Vista parcial'),
          ],
          'help' => __d( 'admin', 'Forma de encajado del slide en la página')
        ],
        'view' => [
          'label' => __d( 'admin', 'Tipo de pase'),
          'type' => 'select',
          'options' => [
            'basic' => __d( 'admin', 'Básico'),
            'fade' => __d( 'admin', 'Fundido'),
            'mask' => __d( 'admin', 'Máscara'),
            'wave' => __d( 'admin', 'Ola'),
            'flow' => __d( 'admin', 'Fluído'),
            'scale' => __d( 'admin', 'Escalar'),
            'focus' => __d( 'admin', 'Focus'),
            'parallaxMask' => __d( 'admin', 'Parallax'),
          ]
        ],
        'slides' => [
          'label' => 'Slides',
          'template' => 'Slider.fields.slides',
          'type' => 'hasMany'
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
    
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Sliders'),
        'plural' => __d( 'admin', 'Sliders'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'width',
                  'height',
                  'auto_height',
                  'layout',
                  'view',
                  'parallax_mode',
                  'speed',
                  'dir',
                  'autoplay',
                  'slider_loop',
                  'shuffle',
                  'slides'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }
}
