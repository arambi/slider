<?php
namespace Slider\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Slider\Model\Entity\Layer;

/**
 * Layers Model
 */
class LayersTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('slider_layers');
    $this->displayField('id');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo'
      ]
    ]);

    $this->addBehavior( 'I18n.I18nable', [
      'fields' => ['body']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Título'),
          'help' => __d( 'admin', 'Título de referencia. No tiene efecto en la visualización.t')
        ],
        'layer_type' => [
          'label' => __d( 'admin', 'Layer Type'),
          'options' => [
            'text' => 'Texto',
            'image' => 'Imagen',
            'button' => 'Botón'
          ]
        ],
        'pos_top' => __d( 'admin', 'Top'),
        'pos_left' => __d( 'admin', 'Left'),
        'parallax_effect' => [
          'label' => __d( 'admin', 'Efecto parallax'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'after' => '%',
          'spin' => true,
        ],
        'transition_offset_x' => [
          'label' => __d( 'admin', 'En X'),
          'type' => 'numeric',
          'range' => [-1000, 1000, 1],
          'spin' => true,
        ],
        'transition_offset_y' => [
          'label' => __d( 'admin', 'En Y'),
          'type' => 'numeric',
          'range' => [-1000, 1000, 1],
          'spin' => true,
        ],
        'transition_offset_z' => [
          'label' => __d( 'admin', 'En Z'),
          'type' => 'numeric',
          'range' => [-1000, 1000, 1],
          'spin' => true,
        ],
        'origin' => __d( 'admin', 'Origin'),
        'effect' => __d( 'admin', 'Effect'),
        'duration' => __d( 'admin', 'Duration'),
        'delay' => __d( 'admin', 'Delay'),
        'transition_name' => [
            'label' => __d( 'admin', 'Efecto'),
            'type' => 'select',
            'options' => [
              'fade' => __d( 'admin', 'Fundido'),
              'left' => __d( 'admin', 'Desde izquierda'),
              'right' => __d( 'admin', 'Desde derecha'),
              'top' => __d( 'admin', 'Desde arriba'),
              'bottom' => __d( 'admin', 'Desde abajo')
            ]
        ],
        'transition_ease' => [
          'label' => __d( 'admin', 'Aceleración'),
          'type' => 'select',
          'options' => [
            'linear',
            'easeInCubic',
            'easeOutCubic',
            'easeInOutCubic',
            'easeInCirc',
            'easeOutCirc',
            'easeInOutCirc',
            'easeInExpo',
            'easeOutExpo',
            'easeInOutExpo',
            'easeInQuad',
            'easeOutQuad',
            'easeInOutQuad',
            'easeInQuart',
            'easeOutQuart',
            'easeInOutQuart',
            'easeInQuint',
            'easeOutQuint',
            'easeInOutQuint',
            'easeInSine',
            'easeOutSine',
            'easeInOutSine',
            'easeInBack',
            'easeOutBack',
            'easeInOutBack'
          ]
        ], 
        'transition_scale_x' => __d( 'admin', 'Escala X'),  
        'transition_scale_y' => __d( 'admin', 'Escala Y'), 
        'transition_fade' => __d( 'admin', 'Con fundido'), 

        // Estilos
        'padding_left' => [
          'label' => __d( 'admin', 'Izquierda'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'padding_right' => [
          'label' => __d( 'admin', 'Derecha'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'padding_top' => [
          'label' => __d( 'admin', 'Arriba'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'padding_bottom' => [
          'label' => __d( 'admin', 'Abajo'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'background_color' => [
          'label' => __d( 'admin', 'Color de fondo'),
          'type' => 'colorpicker'
        ],
        'foreground_color' => [
          'label' => __d( 'admin', 'Color de texto'),
          'type' => 'colorpicker'
        ],
        'width' => [
          'label' => __d( 'admin', 'Anchura de caja'),
          'type' => 'numeric',
          'range' => [0, 1000, 1],
          'spin' => true,
        ],
        'text_shadow_angle' => [
          'label' => __d( 'admin', 'Ángulo'),
          'type' => 'numeric',
          'range' => [0, 1000, 1],
          'after' => 'º',
          'spin' => true,
        ],
        'text_shadow_distance' => [
          'label' => __d( 'admin', 'Distancia'),
          'type' => 'numeric',
          'range' => [-30, 30, 1],
          'after' => 'px',
          'spin' => true,
        ],
        'text_shadow_blur' => [
          'label' => __d( 'admin', 'Blur'),
          'type' => 'numeric',
          'range' => [0, 30, 1],
          'after' => 'px',
          'spin' => true,
        ],
        'text_shadow_opacity' => [
          'label' => __d( 'admin', 'Opacidad'),
          'type' => 'numeric',
          'range' => [0, 10, 1],
          'spin' => true,
        ],
        'border_color' => [
          'label' => __d( 'admin', 'Color'),
          'type' => 'colorpicker'
        ],
        'border_top_width' => [
          'label' => __d( 'admin', 'Arriba'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'border_bottom_width' => [
          'label' => __d( 'admin', 'Abajo'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'border_left_width' => [
          'label' => __d( 'admin', 'Izquierda'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'border_right_width' => [
          'label' => __d( 'admin', 'Derecha'),
          'type' => 'numeric',
          'range' => [0, 100, 1],
          'spin' => true,
        ],
        'border_style' => [
          'label' => __d( 'admin', 'Estilo'),
          'type' => 'select',
          'options' => [
            'solid' => __d( 'admin', 'Sólido'),
            'dotted' => __d( 'admin', 'Punteado'),
            'dashed' => __d( 'admin', 'Rayas')
          ]
        ],
        'border_round' => [
          'label' => __d( 'admin', 'Redondeado'),
          'type' => 'numeric',
          'range' => [0, 10, 1],
          'spin' => true,
        ],
        'line_height' => [
          'label' => __d( 'admin', 'Altura de línea'),
          'type' => 'numeric',
          'range' => [50, 400, 5],
          'after' => '%',
          'spin' => true,
        ],
        'body' => [
          'label' => __d( 'admin', 'Cuerpo de texto'),
          'type' => 'ckeditor'
        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'layer_type',        
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Capa'),
        'plural' => __d( 'admin', 'Capas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'layer_type',
                  'pos_top',
                  'pos_left',
                  'offset_x',
                  'offset_y',
                  'origin',
                  'effect',
                  'duration',
                  'delay',
                  'ease',
                  'body',
                  'photo'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }
}
