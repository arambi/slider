<?php
namespace Slider\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Slider\Model\Entity\Slide;

/**
 * Slides Model
 */
class SlidesTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('slider_slides');
    $this->displayField('id');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo'
      ]
    ]);

    // Asociations
    $this->hasMany( 'Layers', [
      'className' => 'Slider.Layers',
      'foreignKey' => 'slide_id',
      'sort' => [
        'Layers.position' => 'desc'
      ]
    ]);
    
    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
        'has_url' => [
          'label' => __d( 'admin', 'Tiene enlace')
        ],
        'url' => [
          'label' => __d( 'admin', 'Enlace'),
        ],
        'url_target' => [
          'label' => __d( 'admin', 'Abrir enlace en'),
          'type' => 'select',
          'options' => [
            '_self' => __d( 'admin', 'En la misma ventana'),
            '_blank' => __d( 'admin', 'En una ventana nueva')
          ],
        ],
        'fill_mode' => [
          'label' => __d( 'admin', 'Fill Mode'),
          'type' => 'select',
          'options' => [
            'fill' => __d( 'admin', 'Rellenar'),
            'fit' => __d( 'admin', 'Ajustar'),
            'stretch' => __d( 'admin', 'Estirar'),
            'center' => __d( 'admin', 'Centrar'),
            'tile' => __d( 'admin', 'Azulejo'),
          ]
        ],
        'delay' => [
          'label' => __d( 'admin', 'Tiempo'),
          'help' => __d( 'admin', 'El tiempo (en segundos) que tarda en aparecer'),
          'type' => 'numeric',
          'range' => [0, 15, 1],
          'spin' => true,
        ],
        'layers' => [
          'label' => 'Layers',
          'template' => 'Slider.fields.layers',
          'type' => 'hasMany'
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'url',
    
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Slides'),
        'plural' => __d( 'admin', 'Slides'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'photo',
                  'has_url',
                  'url',
                  'url_target',
                  'fill_mode',
                  'delay',
                  'layers'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
    
  }
}
