<?php

namespace Slider\Model\Block;
use Cake\ORM\TableRegistry;

class SliderBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Slider'),
          'plural' => __d( 'admin', 'Slider'),
        ])
      ->addFields([
        'parent_id' => [
          'label' => __d( 'admin', 'Slider'),
          'type' => 'select',
          'options' => function( $crud){
            $return = TableRegistry::get( 'Slider.Sliders')->find( 'list')->toArray();
            return $return;
          }
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'parent_id',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}