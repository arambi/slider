<?php
namespace Slider\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;

/**
 * Slide Entity.
 */
class Slide extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    'salt' => true,
    'slider_id' => true,
    'has_url' => true,
    'url' => true,
    'url_target' => true,
    'fill_mode' => true,
    'delay' => true,
    'photo' => true,
    'layers' => true
  ];
}
