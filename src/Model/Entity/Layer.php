<?php
namespace Slider\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Layer Entity.
 */
class Layer extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'salt' => true,
        'slide_id' => true,
        'layer_type' => true,
        'pos_top' => true,
        'pos_left' => true,
        'offset_x' => true,
        'offset_y' => true,
        'origin' => true,
        'effect' => true,
        'duration' => true,
        'delay' => true,
        'ease' => true,
        'body' => true,
        'photo' => true,
        'position' => true,
        'parallax_effect' => true,
        'transition_name' => true,
        'transition_ease' => true,
        'transition_offset_x' => true,
        'transition_offset_y' => true,
        'transition_offset_z' => true,
        'transition_scale_x' => true, 
        'transition_scale_y' => true, 
        'padding_left' => true,
        'padding_right' => true,
        'padding_top' => true,
        'padding_bottom' => true,
        'background_color' => true,
        'border_color' => true,
        'border_top_width' => true,
        'border_bottom_width' => true,
        'border_left_width' => true,
        'border_right_width' => true,
        'border_style' => true,
        'border_round' => true,
        'foreground_color' => true,
        'width' => true,
        'text_shadow_angle' => true,
        'text_shadow_distance' => true,
        'text_shadow_blur' => true,
        'text_shadow_opacity' => true,
        'line_height' => true,
    ];
}
