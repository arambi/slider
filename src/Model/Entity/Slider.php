<?php
namespace Slider\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * Slider Entity.
 */
class Slider extends Entity
{
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'salt' => true,
        'height' => true,
        'width' => true,
        'auto_height' => true,
        'layout' => true,
        'view' => true,
        'parallax_mode' => true,
        'speed' => true,
        'dir' => true,
        'autoplay' => true,
        'loop' => true,
        'shuffle_order' => true,
        'slides' => true
    ];
}
