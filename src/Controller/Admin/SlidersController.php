<?php
namespace Slider\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Slider\Controller\AppController;

/**
 * Sliders Controller
 *
 * @property \Slider\Model\Table\SlidersTable $Sliders
 */
class SlidersController extends AppController
{
    use CrudControllerTrait;
}
