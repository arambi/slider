<?php
namespace Slider\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Slider\Controller\AppController;

/**
 * Slides Controller
 *
 * @property \Slider\Model\Table\SlidesTable $Slides
 */
class SlidesController extends AppController
{
    use CrudControllerTrait;
}
