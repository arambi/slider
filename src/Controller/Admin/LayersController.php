<?php
namespace Slider\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Slider\Controller\AppController;

/**
 * Layers Controller
 *
 * @property \Slider\Model\Table\LayersTable $Layers
 */
class LayersController extends AppController
{
  use CrudControllerTrait;

  protected function _beforeCreate()
  {
    if( isset( $this->request->params ['pass'][0]))
    {
      $this->Table->crud->defaults( [
        'layer_type' => $this->request->params ['pass'][0]
      ]);
    }
  }
}
